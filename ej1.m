im1 = imread('f1.jpg');
im2 = imread('f2.jpg');
subplot(2,2,1);
imshow(im1);
title('MENOS ILUMINADA');
subplot(2,2,2);
imshow(im2);
title('MAS ILUMINADA');
[R,C,N] = size(im1);
imd = double(im1);
imtp1= imd;
for j=1:R
   for i=1:C
     if( (  imd(j,i,1) + imd(j,i,2) + imd(j,i,3)  ) == 0 )
       imtp1(j,i,1) = 255;
       imtp1(j,i,2)= 255;
       imtp1(j,i,3)=255;
     else  
       imtp1(j,i,1) = imd(j, i,  1) / (  imd(j,i,1) + imd(j,i,2) + imd(j,i,3)  )*255;
       imtp1(j,i,2) = imd(j, i,  2) / (  imd(j,i,1) + imd(j,i,2) + imd(j,i,3)  )*255;
       imtp1(j,i,3) = imd(j, i,  3) / (  imd(j,i,1) + imd(j,i,2) + imd(j,i,3)  )*255;
     end;  
    end;
end;
subplot(2,2,3);
imshow( uint8(imtp1) );


[R,C,N] = size(im2);
imd = double(im2);
imtp2= imd;
for j=1:R
   for i=1:C
     if( (  imd(j,i,1) + imd(j,i,2) + imd(j,i,3)  ) == 0 )
       imtp2(j,i,1) = 255;
       imtp2(j,i,2)= 255;
       imtp2(j,i,3)=255;
     else  
       imtp2(j,i,1) = imd(j, i,  1) / (  imd(j,i,1) + imd(j,i,2) + imd(j,i,3)  )*255;
       imtp2(j,i,2) = imd(j, i,  2) / (  imd(j,i,1) + imd(j,i,2) + imd(j,i,3)  )*255;
       imtp2(j,i,3) = imd(j, i,  3) / (  imd(j,i,1) + imd(j,i,2) + imd(j,i,3)  )*255;
     end;  
    end;
end;
subplot(2,2,4);
imshow( uint8(imtp2) );